/*jshint unused:false */

/***************

 This file allow to configure a proxy system plugged into BrowserSync
 in order to redirect backend requests while still serving and watching
 files from the web project

 IMPORTANT: The proxy is disabled by default.

 If you want to enable it, watch at the configuration options and finally
 change the `module.exports` at the end of the file

 ***************/

'use strict';

var httpProxy = require('http-proxy');
var chalk = require('chalk');

//ServiceNow API authentication
var snConfig = '../sn.conf.js';
var sn = require(snConfig);
var auth =  new Buffer(sn.config.username + ':' + sn.config.password).toString('base64');

/*
 * Location of your backend server
 */
var proxyTarget = 'https://' + sn.config.instance;

var proxy = httpProxy.createProxyServer({
    target: proxyTarget,
    secure: true,
    changeOrigin: true
});

proxy.on('proxyReq', function(proxyReq, req, res, options) {
    if (sessionToken) {
        proxyReq.setHeader('X-UserToken', sessionToken);
    }
    if (sessionCookie) {
        proxyReq.setHeader('Cookie', sessionCookie);
    }
    proxyReq.setHeader('Authorization', 'Basic ' + auth);
});

proxy.on('error', function (error, req, res) {
    res.writeHead(500, {
        'Content-Type': 'text/plain'
    });

    console.error(chalk.red('[Proxy]'), error);
});

var sessionToken = '';
var sessionCookie = '';

proxy.on('proxyRes', function (proxyRes, req, res) {
    if (req.url === '/x_mobi_p_session_token.do') {
        var c = proxyRes.headers['set-cookie'];
        if (Array.isArray(c)) {
            var data = c.join('; ');
            if (data.indexOf('JSESSIONID') !== -1) {
                sessionCookie = data;
            }
        }
        var body = '';
        proxyRes.on('data', function(chunk) {
            body += chunk;
        });

        proxyRes.on('end', function () {
            sessionToken = body;
        });
    }
});

/*
 * The proxy middleware is an Express middleware added to BrowserSync to
 * handle backend request and proxy them to your backend.
 */
function proxyMiddleware(req, res, next) {
    if (isServiceNowCall(req.url)) {
        //only SN mode is supported now
        //proxy call to ServiceNow to avoid problems with Access-Control-Allow-Origin
        //console.log('Proxy: ' + req.url + ' > ' + proxyTarget);
        proxy.web(req, res);
    } else {
        next();
    }
}

function isServiceNowCall(url) {
    //REST API: api/x
    if (/\/api\/x/g.test(url)) {
        return true;
    }
    //REST API: api/now/table
    if (/\/api\/now\/table/g.test(url)) {
        return true;
    }
    //REST API stats: api/now/stats
    if (/\/api\/now\/stats/g.test(url)) {
        return true;
    }
    //REST API attachment: api/now/attachment
    if (/\/api\/now\/attachment/g.test(url)) {
        return true;
    }
    //Images: *.iix
    else if (/.(iix)/.test(url)) {
        return true;
    }
    //*.js*x
    else if (/.(js[a-z]*x)/.test(url)) {
        return true;
    }
    //*.css*x
    else if (/.(css[a-z]*x)/.test(url)) {
        return true;
    }
    //Direct URL
    else if (/\.(do)/i.test(url)) {
        return true;
    }
    //Processors: x_mobi_p_api_*
    else if (/x_mobi_p_api/.test(url)) {
        return true;
    }
    return false;
}

/*
 * This is where you activate or not your proxy.
 *
 * The first line activate if and the second one ignored it
 */

module.exports = [proxyMiddleware];
