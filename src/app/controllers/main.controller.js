export class MainController {
  constructor ($timeout, ExcelService) {
    'ngInject';
    this.$timeout = $timeout;
    this.Excel = ExcelService;
  }

  exportToExcel(tableId){
    this.exportHref=this.Excel.tableToExcel(tableId,'Incidents list');
    this.$timeout(()=>{location.href=this.exportHref;},100);
  }

}
