

import { config } from './index.config';
import { runBlock } from './index.run';
import { MainController } from './controllers/main.controller.js';


import { IncidentsService } from './services/incidents.service.js';
import { UserService } from './services/user.service.js';
import { ExcelService } from './services/excel.service.js';
import { incidentsTableDirective } from './components/incidentsTable/incidentsTable.directive';
import { incidentsByStateDirective } from './components/incidentsByState/incidentsByState.directive';
import { incidentsOverTimeDirective } from './components/incidentsOverTime/incidentsOverTime.directive';
import { ModalController } from '../app/components/modal/modal.controller';
import { API_PARAMS } from '../app/constants/auth.constant';

angular.module('myTestProject', [
  'ui.bootstrap',
  'toastr',
  'chart.js'
])
  .constant('API_PARAMS', API_PARAMS)
  .config(config)
  .run(runBlock)
  .controller('MainController', MainController)
  .controller('ModalController', ModalController)
  .service('incidentsService', IncidentsService)
  .service('userService', UserService)
  .service('ExcelService', ExcelService)
  .directive('incidentsTable', incidentsTableDirective)
  .directive('incidentsByState', incidentsByStateDirective)
  .directive('incidentsOverTime', incidentsOverTimeDirective)
  .filter('myOwnFormat', ()=>{
      return (text)=>{
        var  d = text.split(' ')[0].split('-');
        return d[2]+'/'+d[1]+'/'+d[0];
      }
  });

