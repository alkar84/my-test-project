export class UserService {
  constructor ($log, $http, API_PARAMS) {
    'ngInject';

    this.$log = $log;
    this.$http = $http;
  }

  getUserName(userId) {
    return this.$http.get('/api/table/sys_user/' + userId,
      { headers: this.authParams }
        ).then((response) => {
          return response.data.result;
      })
      .catch((error) => {
        this.$log.error('XHR Failed.\n' + angular.toJson(error.data, true));
      });
  }
}
