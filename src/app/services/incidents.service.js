export class IncidentsService {
  constructor ($log, $http, userService, API_PARAMS) {
    'ngInject';
    this.UserService = userService;
    this.$log = $log;
    this.$http = $http;

    this.incidents = {};
    this.incidentsForChart = {};

    this.showLoadBar = {};

    this.incidentsForChart.byStateTitle = [];
    this.incidentsForChart.byState = [];
    this.incidentsForChart.byDateTitle = [];
    this.incidentsForChart.byDate = [];

    this.selected = {};
    this.updateIncidents();
  }

  loadMore(){
    this.showLoadBar.table = true;
    //this.$log.info('loadMore started')
    var count = this.incidents.data.length;
    this.$http.get('/api/now/table/incident?sysparm_display_value=true&sysparm_offset='+count+'&sysparm_limit=10',
      { headers: this.authParams }).then((response) => {

        this.incidents.data = this.incidents.data.concat(response.data.result)
        this.chartUpdate(this.incidents.data);
        this.showLoadBar.table = false;
    })

  }

  updateIncidents(sysparm_limit=10) {
    this.$log.info('this.showLoadBar',this.showLoadBar)
    this.showLoadBar.table = true;
    //this.$log.info('setIncidents start this.incidents', this.incidents)
    return this.$http.get('/api/now/table/incident?sysparm_display_value=true&sysparm_limit=' + sysparm_limit,
      { headers: this.authParams }
    ).then((response) => {
      this.chartUpdate(response.data.result);
      this.incidents.data = response.data.result;
      this.showLoadBar.table = false;
        /* if sysparm_display_value=false
        this.incidents.data = response.data.result.map((item)=>{
          var res = {};

          res.number = item.number;
          res.urgency = item.urgency;
          res.caller_name = item.opened_by.display_value;
          res.opened_at = item.opened_at;
          res.short_description = item.short_description;
          res.description = item.description;
          res.user_url = item.opened_by.link;
          res.state = item.state;
            this.UserService.getUserName(item.caller_id.value).then((data)=>{
              res.caller_name = data.name;
            });
          //this.$log.info('done map')
          return res;
          })
        */
      //this.$log.info('setIncidents stop this.incidents.data', this.incidents.data)
    })
      .catch((error) => {
        this.$log.error('XHR Failed.\n' + angular.toJson(error.data, true));
        this.showLoadBar.table = false;
      });
  }

  chartUpdate(data){
    //this.$log.info('chartUpdate data',data)
    // this.$log.info('this.incidentsForChart.byStateTitle',this.incidentsForChart.byStateTitle)
    // this.$log.info('this.incidentsForChart.byState',this.incidentsForChart.byState)

    var obj = {};
    var objData = [];

    var objByDate = {};
    var objDateData = [];

    for(let k of data){
      //this.$log.info('kkkkkkk',k.number)
      if(obj[k.state] == undefined){
        obj[k.state] = 1;
      } else {
        obj[k.state]++;
      }

      // second chart
      let second_chart = k.opened_at.split(' ')[0];
      if( objByDate[second_chart] == undefined){
        objByDate[second_chart] = 1;
      } else {
        objByDate[second_chart]++;
      }
    }

    let title = Object.keys(obj);
    title.map((key)=>{
      objData.push(obj[key]);
    });

    let titleDate = Object.keys(objByDate);
    titleDate.map((key)=>{
      objDateData.push(objByDate[key]);
    });
    //
    // this.$log.info('title',title)
    // this.$log.info('objData',objData)


    this.incidentsForChart.byStateTitle = title
    this.incidentsForChart.byState = objData;

    this.incidentsForChart.byDateTitle = titleDate;
    this.incidentsForChart.byDate = objDateData;
}

}
