export class ExcelService {
  constructor($window) {
    'ngInject';
    this.$window = $window;

    this.uri = 'data:application/vnd.ms-excel;base64,';
    this.template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" ' +
        'xmlns:x="urn:schemas-microsoft-com:office:excel" ' +
        'xmlns="http://www.w3.org/TR/REC-html40"><head>' +
        '<!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets>' +
        '<x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions>' +
        '<x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet>' +
        '</x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head>' +
        '<body><table>{table}</table></body></html>';
    this.base64 = (s)=> {
        return $window.btoa(unescape(encodeURIComponent(s)));
    }
    this.format = (s, c)=> {
        return s.replace(/{(\w+)}/g, (m, p)=> {return c[p];})
    };
  }

  tableToExcel( tableId, worksheetName ){
      var table = angular.element( document.getElementById( tableId) );
      var ctx = { worksheet:worksheetName, table:table.html() };
      var href = this.uri+this.base64(this.format(this.template, ctx));

    return href;
  }

}


