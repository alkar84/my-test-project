export function incidentsOverTimeDirective() {
  'ngInject';

  let directive = {
    restrict: 'E',
    scope: {
        extraValues: '='
    },
    templateUrl: 'app/components/incidentsOverTime/incidentsOverTime.html',
    controller: IncidentsOverTimeController,
    controllerAs: 'incTime'
  };

  return directive;
}

class IncidentsOverTimeController {
  constructor ($log,  incidentsService) {
    'ngInject';
    this.$log = $log;

    this.incidentsService = incidentsService;
    this.chartDate = this.incidentsService.incidentsForChart;

  }


}
