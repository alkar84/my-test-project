export function incidentsByStateDirective() {
  'ngInject';

  let directive = {
    restrict: 'E',
    scope: {
        extraValues: '='
    },
    templateUrl: 'app/components/IncidentsByState/IncidentsByState.html',
    controller: IncidentsByStateController,
    controllerAs: 'incState'
  };

  return directive;
}

class IncidentsByStateController {
  constructor ($log,  incidentsService) {
    'ngInject';
    this.$log = $log;

    this.incidentsService = incidentsService;
    this.chartDate = this.incidentsService.incidentsForChart;
    this.allIncidents = this.incidentsService.incidents;


  }



}
