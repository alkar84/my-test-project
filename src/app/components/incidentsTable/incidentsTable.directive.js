export function incidentsTableDirective() {
  'ngInject';

  let directive = {
    restrict: 'E',
    scope: {
        extraValues: '='
    },
    templateUrl: 'app/components/IncidentsTable/IncidentsTable.html',
    controller: IncidentsTableController,
    controllerAs: 'incTable'
  };

  return directive;
}

class IncidentsTableController {
  constructor ($log, $uibModal, incidentsService) {
    'ngInject';
    this.$log = $log;
    this.$uibModal = $uibModal;


    this.incidentsService = incidentsService;
    this.allIncidents = this.incidentsService.incidents;
    this.showLoader = this.incidentsService.showLoadBar;

    this.animationsEnabled = true;
   }

  showFullView(incident) {
    this.incidentsService.selected = incident;
    this.$uibModal.open({
      templateUrl: 'app/components/modal/modal.html',
      controller: 'ModalController',
      controllerAs: 'modalCtrl'
    });
  }

  loadMore(){
    this.incidentsService.loadMore();
  }

  customSearch(item){
    var s = angular.element( document.getElementById('search') );
    s = s.val().toLowerCase();

    if(s == undefined){
      return true
    } else {
      if(item.number.toLowerCase().indexOf(s) != -1 ||
        (item.opened_by.display_value && item.opened_by.display_value.toLowerCase().indexOf(s) != -1)){
        return true
      }
    }
    return false
  }

  customSearchSel(item){
    var s = angular.element( document.getElementById('sel2') );
    s = s.val().toLowerCase();
    //console.log('s',s)
    if(s == undefined){
      return true
    } else {
      if(item.urgency.toLowerCase().indexOf(s) != -1){
        return true
      }
    }
    return false
  }

  customSearchSel2(item){
    var s = angular.element( document.getElementById('sel1') );
    s = s.val().toLowerCase();
    //console.log('s',s)
    if(s == undefined){
      return true
    } else {
      if(item.state.toLowerCase().indexOf(s) != -1){
        return true
      }
    }
    return false
  }
}
