export function modalDirective() {
  'ngInject';

  let directive = {
    templateUrl: 'myModalContent.html',
    bindings: {
      resolve: '<',
      close: '&',
      dismiss: '&'
    }
  }

  return directive;
}

