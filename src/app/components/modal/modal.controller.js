export class ModalController {
  constructor ($log, $uibModalStack, incidentsService) {
    'ngInject';

    this.$uibModalStack = $uibModalStack;
    this.incidentsService = incidentsService;
    this.selected = this.incidentsService.selected;

    $log.info('selected',this.selected)
  }

  cancel() {
    this.$uibModalStack.dismissAll()
  }

}
